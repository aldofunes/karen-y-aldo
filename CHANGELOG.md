# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.9](https://gitlab.com/aldofunes/karen-y-aldo/compare/v0.1.8...v0.1.9) (2019-08-07)


### Features

* add confirm venue sections ([f416f9f](https://gitlab.com/aldofunes/karen-y-aldo/commit/f416f9f))

### [0.1.8](https://gitlab.com/aldofunes/karen-y-aldo/compare/v0.1.7...v0.1.8) (2019-08-06)


### Features

* update favicon ([98b8322](https://gitlab.com/aldofunes/karen-y-aldo/commit/98b8322))

### [0.1.7](https://gitlab.com/aldofunes/karen-y-aldo/compare/v0.1.6...v0.1.7) (2019-08-06)


### Features

* venue details ([fb6072e](https://gitlab.com/aldofunes/karen-y-aldo/commit/fb6072e))

### [0.1.6](https://gitlab.com/aldofunes/karen-y-aldo/compare/v0.1.5...v0.1.6) (2019-08-02)


### Bug Fixes

* mobile flex wrap ([6f507cc](https://gitlab.com/aldofunes/karen-y-aldo/commit/6f507cc))

### [0.1.5](https://gitlab.com/aldofunes/karen-y-aldo/compare/v0.1.4...v0.1.5) (2019-08-02)


### Features

* use bodakarenyaldo as hasthag ([2527be9](https://gitlab.com/aldofunes/karen-y-aldo/commit/2527be9))

### [0.1.4](https://gitlab.com/aldofunes/karen-y-aldo/compare/v0.1.3...v0.1.4) (2019-08-02)


### Bug Fixes

* about us responsiveness ([1511edf](https://gitlab.com/aldofunes/karen-y-aldo/commit/1511edf))

### [0.1.3](https://gitlab.com/aldofunes/karen-y-aldo/compare/v0.1.2...v0.1.3) (2019-08-02)


### Features

* home and rsvp ([8af90a3](https://gitlab.com/aldofunes/karen-y-aldo/commit/8af90a3))
* photo gallery ([6e567d2](https://gitlab.com/aldofunes/karen-y-aldo/commit/6e567d2))
* single page app ([eea2517](https://gitlab.com/aldofunes/karen-y-aldo/commit/eea2517))

### [0.1.2](https://gitlab.com/aldofunes/karen-y-aldo/compare/v0.1.0...v0.1.2) (2019-08-02)

### [0.1.1](https://gitlab.com/aldofunes/karen-y-aldo/compare/v0.1.0...v0.1.1) (2019-08-02)

## 0.1.0 (2019-08-02)
