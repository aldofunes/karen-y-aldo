// @flow
import React from 'react';

import laura from './laura.jpg';
import rafael from './rafael.jpg';
import alejandra from './alejandra.jpg';
import miguel from './miguel.jpg';

const Families = () => (
  <section className="pa3">
    <div className="flex items-center justify-around ma4 flex-column flex-row-l">
      <h1 className="f1 fw3 avenir light-red ma0">De la novia</h1>
      <div className="mv3">
        <img
          className="br-100 w5 h5 shadow-1 ba b--white bw3"
          src={alejandra}
          alt="Alejandra Velasco"
        />
        <h2 className="avenir tc f2 fw2 ma0">Alejandra Velasco</h2>
      </div>
      <div className="mv3">
        <img
          className="br-100 w5 h5 shadow-1 ba b--white bw3"
          src={miguel}
          alt="Miguel Vázquez"
        />
        <h2 className="avenir tc f2 fw2 ma0">Miguel Vázquez</h2>
      </div>
    </div>

    <div className="flex items-center justify-around ma4 flex-column flex-row-l">
      <h1 className="f1 fw3 avenir light-teal ma0">Del novio</h1>
      <div className="mv3">
        <img
          className="br-100 w5 h5 shadow-1 ba b--white bw3"
          src={laura}
          alt="Laura Minutti"
        />
        <h2 className="avenir tc f2 fw2 ma0">Laura Minutti</h2>
      </div>
      <div className="mv3">
        <img
          className="br-100 w5 h5 shadow-1 ba b--white bw3"
          src={rafael}
          alt="Rafael Funes"
        />
        <h2 className="avenir tc f2 fw2 ma0">Rafael Funes</h2>
      </div>
    </div>
  </section>
);

export default Families;
