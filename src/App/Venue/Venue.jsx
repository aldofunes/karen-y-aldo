// @flow
import React from 'react';
import groomBride from './groom-bride.jpg';
import reception from './reception.jpg';
import croquis from './croquis.jpg';

import { Quote } from 'components';

const Venue = () => (
  <section className="ph3">
    <Quote
      quote="Cuando te das cuenta que quieres pasar el resto de tu vida con una persona, quieres que el resto de tu vida empiece lo antes posible."
      author="Billy Cristal"
    />
    <div className="flex flex-wrap">
      <div className="w-100 w-50-l pa3">
        <img src={groomBride} alt="Ceremonia religiosa" />
        <h2 className="f2 fw3 avenir light-teal ma0">Ceremonia religiosa</h2>
        <h4 className="f4 fw3 avenir mv2">12:30pm</h4>
        <p className="center avenir ma0">
          Te invitamos a que nos acompañes a celebrar nuestro matrimonio.
        </p>
      </div>

      <div className="w-100 w-50-l pa3">
        <img src={reception} alt="Recepción" />
        <h2 className="f2 fw3 avenir light-teal ma0">Recepción</h2>
        <p className="center avenir">
          Terminando la ceremonia, esperamos que puedan unirse a nosotros para
          celebrar nuestra vida juntos y disfrutar en compañía de nuestros
          amigos y familiares.
        </p>
      </div>
    </div>

    <div className="pv4 tc">
      <h3 className="f3 fw3 avenir light-teal ma0">Jardín Rincón Dorado</h3>
      <p className="lh-copy">
        Carr. Zapata - Zacatepec (Av. Emiliano Zapata) Km. 13
        <br />
        El Pedregal, 62795, Chiconcuac, Morelos
      </p>
      <div className="flex items-center justify-center">
        <a
          href="https://goo.gl/maps/wr5DRnbVSmHAuqYy5"
          target="_blank"
          rel="noopener noreferrer"
          className="bg-near-white black sans-serif pv2 ph3 br-pill link grow mh1"
        >
          Google Maps
        </a>
        <a
          href="https://www.waze.com/ul?ll=18.78886020%2C-99.20258400&navigate=yes"
          target="_blank"
          rel="noopener noreferrer"
          className="bg-near-white black sans-serif pv2 ph3 br-pill link grow mh1"
        >
          Waze
        </a>
        <a
          href={croquis}
          target="_blank"
          rel="noopener noreferrer"
          className="bg-near-white black sans-serif pv2 ph3 br-pill link grow mh1"
        >
          Croquis
        </a>
      </div>
    </div>
  </section>
);

export default Venue;
