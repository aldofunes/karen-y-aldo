// @flow
import React from 'react';
import styles from './Confirm.module.css';
import WhatsApp from './WhatsApp.png';

type Props = {
  openRsvp: () => void
};

const Confirm = ({ openRsvp }: Props) => {
  const aldoUrl = new URL('https://wa.me/525532234680');

  aldoUrl.searchParams.set(
    'text',
    'Hola, \n\nQuiero confirmar mi asistencia a la boda de Karen y Aldo, el 24 de julio, 2021.\n\nMi nombre completo es:\n'
  );
  const karenUrl = new URL('https://wa.me/525585306428');

  karenUrl.searchParams.set(
    'text',
    'Hola, \n\nQuiero confirmar mi asistencia a la boda de Karen y Aldo, el 24 de julio, 2021.\n\nMi nombre completo es:\n'
  );

  return (
    <div
      className={`${styles.background} pv5 flex items-center justify-center`}
    >
      <div className="bw3 pa3 ba b--white-80 mw-100 ">
        <div className="bg-white-80 pa4 sans-serif tc mw-100">
          <h2 className="fw3 f2 mb2 mt0">Confirma tu asistencia</h2>
          <h5 className="fw3 f5 mt2">24 de julio, 2021 (Sin niños)</h5>
          <svg
            className="light-red db w-100"
            id="chart"
            width="500"
            height="60"
            viewBox="0 0 500 60"
          >
            <line
              strokeWidth="1"
              stroke="black"
              x1="0"
              y1="30"
              x2="200"
              y2="30"
            />
            <path
              fill="currentColor"
              transform="scale(2)"
              d="M125 7c-1.989-5.399-12-4.597-12 3.568 0 4.068 3.06 9.481 12 14.997 8.94-5.516 12-10.929 12-14.997 0-8.118-10-8.999-12-3.568z"
            />
            <line
              strokeWidth="1"
              stroke="black"
              x1="300"
              y1="30"
              x2="500"
              y2="30"
            />
          </svg>

          <a
            href="https://karenyaldo.app.rsvpify.com/"
            target="_blank"
            rel="noopener noreferrer"
            className="db link white center bg-light-red pv2 ph3 pointer w-100 bn grow br-pill mt3"
          >
            RSVP
          </a>

          <p>
            Con Aldo{' '}
            <a className="link blue" href="tel:+525532234680">
              +52 (55) 3223 4680
            </a>
            <a className="link dim blue db pa3" href={aldoUrl.href}>
              <div className="flex items-center justify-center">
                <div className="mr2">
                  <img className="w2" src={WhatsApp} alt="WhatsApp" />
                </div>
                <div>WhatsApp</div>
              </div>
            </a>
          </p>
          <p>
            Con Karen{' '}
            <a className="link blue" href="tel:+525585306428">
              +52 (55) 8530 6428
            </a>
            <a className="link dim blue db pa3" href={karenUrl.href}>
              <div className="flex items-center justify-center">
                <div className="mr2">
                  <img className="w2" src={WhatsApp} alt="WhatsApp" />
                </div>
                <div>WhatsApp</div>
              </div>
            </a>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Confirm;
