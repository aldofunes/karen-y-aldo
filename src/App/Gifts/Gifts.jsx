// @flow
import { Quote } from 'components';
import React from 'react';
import gifts from './gifts.jpg';
import travel from './travel.jpg';

const Gifts = () => (
  <section id="regalos" className="pa3">
    <div className="flex items-stretch justify-between flex-column flex-row-l">
      <div
        className="flex flex-column justify-between pa3 white bg-center cover br3"
        style={{ backgroundImage: `url(${gifts})` }}
      >
        <p className="f4 db">
          Para aquellos que quieran ayudarnos a llenar nuestra casa de cosillas
          útiles, les dejamos nuestras mesas de regalos. Lo que compres nos
          llegará de sorpresa a nuestra casita.
        </p>
        <div className="flex items-center justify-around">
          <a
            className="link pv2 ph3 bg-light-red white br2 black sans-serif dim ma1 db"
            href="https://www.amazon.com.mx/wedding/share/karenyaldo"
            rel="noopener noreferrer"
            target="_blank"
          >
            Mesa de regalos de Amazon
          </a>
          <a
            className="link pv2 ph3 bg-light-red white br2 black sans-serif dim ma1 db"
            href="https://mesaderegalos.liverpool.com.mx/milistaderegalos/50256152"
            rel="noopener noreferrer"
            target="_blank"
          >
            Mesa de regalos de Liverpool
          </a>
        </div>
      </div>
      <div className="pa3">
        <p></p>
      </div>
      <div
        className="flex flex-column justify-between pa3 white bg-center cover br3"
        style={{ backgroundImage: `url(${travel})` }}
      >
        <p className="f4 db">
          Para los que, por otro lado, prefieran ayudarnos con los gastos de las
          mil y un cosas que hacemos y que nos apasionan, les dejamos nuestra
          cuenta para que nos depositen o transfieran:
        </p>
        <p className="f5 tc ma0 db">
          Banco: <strong>BBVA</strong>
          <br />
          Nombre: <strong>Aldo Funes Minutti</strong>
          <br />
          Cuenta: <strong>1421 477 919</strong>
          <br />
          CLABE: <strong>012 180 014 214 779 199</strong>
        </p>
      </div>
    </div>

    <Quote
      author="George Sand"
      quote="Te amo para amarte y no para ser amado, puesto que nada me place tanto como verte feliz."
    />
  </section>
);

export default Gifts;
