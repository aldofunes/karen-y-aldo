// @flow
import React from 'react';
import dressCode from './dress-code.jpg';

const DressCode = () => (
  <section className="pa3 bg-near-white avenir">
    <div className="flex justify-around items-center tc fw3 flex-wrap flex-shrink-0">
      <div className="w-100 w-33-ns mw6 flex-shrink-0 order-0 order-1-ns">
        <img className="w-100" src={dressCode} alt="Formal Guayabera" />
      </div>
      <div className="w-100 w-33-ns order-1 order-0-ns">
        <h3 className="fw3 f3">Mujeres</h3>
        <h4 className="fw3 f4">Vestido largo vaporoso</h4>
      </div>
      <div className="w-100 w-33-ns order-2">
        <h3 className="fw3 f3">Hombres</h3>
        <h4 className="fw3 f4">Formal Guayabera</h4>
      </div>
    </div>
  </section>
);

export default DressCode;
