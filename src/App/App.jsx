// @flow
import React, { useState } from 'react';

import { Modal, Parallax } from 'components';

import Accommodations from './Accommodations';
import Confirm from './Confirm';
import DressCode from './DressCode';
import Header from './Header';
import AboutUs from './AboutUs';
// import Photos from './Photos';
import Gifts from './Gifts';
import Families from './Families';
import Rsvp from './Rsvp';
import Venue from './Venue';
import IMG_1937 from './images/IMG_1937.jpg';
import _5D30619 from './images/_5D30619.jpg';

const HASHTAG = 'bodakarenyaldo';

const App = () => {
  const [showRsvp, toggleRsvp] = useState(false);

  return (
    <>
      <Header />

      <AboutUs />
      <Parallax imageUrl={IMG_1937} />
      <Families />
      <Confirm openRsvp={() => toggleRsvp(true)} />
      <Venue />
      <DressCode />

      <Gifts />
      <Parallax imageUrl={_5D30619} />
      <Accommodations />

      {/* <Photos /> */}

      <section>
        <div className="ph3">
          <h2 className="tc avenir fw3 mv4">
            Etiqueta tus fotos en Instagram con{' '}
            <a
              className="link dim light-teal"
              target="_blank"
              rel="noopener noreferrer"
              href={`https://www.instagram.com/explore/tags/${HASHTAG}/`}
            >
              #{HASHTAG}
            </a>
          </h2>
        </div>
      </section>

      {showRsvp && (
        <Modal onClose={() => toggleRsvp(false)}>
          <Rsvp />
        </Modal>
      )}
    </>
  );
};

export default App;
