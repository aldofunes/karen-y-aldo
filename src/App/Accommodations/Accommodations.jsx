// @flow
import React from 'react';
import Hotel from './Hotel';
import nearbyHotels from './nearby-hotels.pdf';
import { ReactComponent as Download } from 'feather-icons/dist/icons/paperclip.svg';

const Accommodations = () => (
  <section className="pa3 bg-near-white">
    <h4 className="avenir fw3 f4">
      Algunos hoteles que te recomendamos para que pases la noche
    </h4>

    <div className="flex flex-wrap items-stretch">
      <Hotel
        address="Carretera Mexico-Acapulco, Km. 88, Delicias, 62330 Cuernavaca, Morelos"
        imageUrl="http://media.staticontent.com/media/pictures/a5a81bd8-3b0d-4bde-9b02-56e6eb259708"
        mapUrl="https://goo.gl/maps/fYqy9RgxQEuCM2Sy7"
        name="Fiesta Inn"
        websiteUrl="https://reservations.fiestamericana.com/97957?datein=05/09/2020&dateout=05/10/2020&languageid=1&children2=0&adults=2&children=0&rooms=1&utmcs=UTF-8&utmfl=0&utmje=0&utmsc=24-bit&utmsr=1440x877&utmul=en-GB&utmdt=Overview&utmp=/Fiesta%20Inn%20Cuernavaca/Overview&utmhn=www.fiestainn.com&utmr=https://www.fiestainn.com/web/fiesta-inn-cuernavaca/overview?p_p_id=travelclickhorizontal_WAR_travelclickhorizontalportlet&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=1&p_p_col_count=2&utmcc=__utma198387512.1537890713.1566357011.1566357011.1566357011.1__utmz198387512.1566357011.1.1.utmcsr&utmac=&_ga=&utm_nooverride=1#/accommodation/room"
      />
      <Hotel
        address="Reforma No. 2 Fracc, Real del Puente, 62790 Chiconcuac, Morelos"
        imageUrl="https://lh5.googleusercontent.com/p/AF1QipOkQGmcMvKvKF49BV4glJqArd_s6k98LhiRaTtH=w408-h272-k-no"
        mapUrl="https://goo.gl/maps/yYqvzw9PNMioAXqE8"
        name="Fiesta Americana"
        websiteUrl="https://reservations.fiestamericana.com/97959?adults=2&children=0&hotelid=97959&datein=05/09/2020&dateout=05/10/2020&languageid=2&utmdt=Fiesta%20Americana&utmp=/fiesta-americana-hacienda-san-antonio-el-puente-cuernavaca&utmhn=www.fiestamericana.com&utm_nooverride=1&utmcs=UTF-8&utmfl=0&utmje=0&utmsc=24-bits&utmsr=1440x877&utmul=en-GB&_ga=GA1.2.1387816942.1566357027&utmcc=__utmanull__utmznull#/accommodation/room"
      />
      <Hotel
        address="Carretera, Zapata - Zacatepec Km 13, Centro, 62766 Morelos"
        imageUrl="https://r-cf.bstatic.com/images/hotel/max1024x768/206/206876911.jpg"
        mapUrl="https://goo.gl/maps/WzQPDC1ExGAKW4rJ7"
        name="Hotel posada el encanto"
        websiteUrl="https://www.booking.com/hotel/mx/posada-el-encanto.en-gb.html?aid=1192690;label=cuform-city-2020-02-25_16%3A28%3A20-ct_ntab-whotel-hotel_posada_el_encanto-w1M1KyrKqfhmDJNHB;sid=5103dec057dbb2363161adb1cf1d1483;all_sr_blocks=175551302_200612543_2_10_0;checkin=2020-05-08;checkout=2020-05-10;dest_id=-1708566;dest_type=city;dist=0;group_adults=2;group_children=0;hapos=1;highlighted_blocks=175551302_200612543_2_10_0;hpos=1;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=175551302_200612543_2_10_0__288000;srepoch=1582644527;srpvid=dd8f6cd68b550229;type=total;ucfs=1&#hotelTmpl"
      />
      <Hotel
        address="Prof. Severo Castro 23, Centro, 62790 Centro, Morelos"
        imageUrl="http://zoviq.com.mx/wp-content/uploads/2018/02/Bungalows-6-personas-10-750x500.jpg"
        mapUrl="https://goo.gl/maps/JmowpHQDdMd54rC67"
        name="Bungalows Zoviq"
        websiteUrl="http://zoviq.com.mx/"
      />
      <Hotel
        address="Vicente Guerrero 32, Centro, 62790 Centro, Morelos"
        imageUrl="https://www.villasalvatore.mx/images/2.jpg"
        mapUrl="https://goo.gl/maps/6Br2Dx8mMvr6yhJb6"
        name="Hotel villas salvatore"
        websiteUrl="https://www.villasalvatore.mx/"
      />
      <Hotel
        address="Autopista Mexico-Acapulco Km 104, Col. Granjas Merida, 62590, Temixco, Morelos"
        imageUrl="https://static.wixstatic.com/media/4bed1c_ad9ce282d840408295a23f444a0bb8e3.jpg/v1/fill/w_812,h_542,al_c,q_85/4bed1c_ad9ce282d840408295a23f444a0bb8e3.webp"
        mapUrl="https://goo.gl/maps/Bu1H4UHvsF91dwRp6"
        name="Hostería San Carlos"
        websiteUrl="https://www.hosteriasancarlos.com.mx/"
      />
    </div>

    <a
      className="db white center bg-light-red pv2 ph3 pointer bn grow br-pill mt3 tc link"
      target="_blank"
      rel="noopener noreferrer"
      href={nearbyHotels}
    >
      Más hoteles cercanos <Download className="w1 h1" />
    </a>
  </section>
);

export default Accommodations;
