// @flow
import { ReactComponent as LinkIcon } from 'feather-icons/dist/icons/external-link.svg';
import { ReactComponent as MapIcon } from 'feather-icons/dist/icons/map-pin.svg';
import React from 'react';

type Props = {
  address: string,
  imageUrl: string,
  mapUrl: string,
  name: string,
  websiteUrl: string
};

const Hotel = ({ address, imageUrl, mapUrl, name, websiteUrl }: Props) => (
  <div className="pa2 w-100 w-50-m w-33-l overflow-hidden helvetica">
    <div
      className="cover bg-center h-100"
      style={{ backgroundImage: `url(${imageUrl})` }}
    >
      <div className="bg-black-60 white pa3 fw3 h-100">
        <div className="flex justify-between">
          <h4 className="f4 mt0 fw4">{name}</h4>

          <div className="flex-shrink-0">
            <a
              className="link bg-near-black grow pa1 dib h2 w2 white br2 mr2"
              href={mapUrl}
              title="Direcciones"
            >
              <MapIcon />
            </a>
            <a
              className="link bg-near-black grow pa1 dib h2 w2 white br2"
              href={websiteUrl}
              title="Sitio Web"
            >
              <LinkIcon />
            </a>
          </div>
        </div>

        <p className="ma0">{address}</p>
      </div>
    </div>
  </div>
);

export default Hotel;
