// @flow
import React from 'react';

import balloon from './balloon2.jpg';

const Header = () => (
  <header
    className="black-80 tc pv4 avenir cover bg-center vh-100 flex items-center justify-center flex-column white"
    style={{ backgroundImage: `url(${balloon})` }}
  >
    <h2 className="mt6 mb0 baskerville i fw1 f2">Karen y Aldo</h2>
    <h1 className="mt2 mb0 avenir fw1 f1">24 de julio, 2021</h1>
    <h2 className="mt2 mb0 f6 fw4 ttu tracked">Nos vamos a casar</h2>
  </header>
);

export default Header;
