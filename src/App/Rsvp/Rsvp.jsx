// @flow
import React, { Component } from 'react';

import { Spinner } from 'components';
import { iframeResizer } from 'iframe-resizer';

class Rsvp extends Component<{}, { loading: boolean }> {
  state = { loading: true };

  componentDidMount() {
    iframeResizer(
      {
        autoResize: true,
        checkOrigin: false,
        heightCalculationMethod: 'bodyScroll'
      },
      '#RSVPifyIFrame'
    );
  }

  onLoad = () => this.setState({ loading: false });

  render() {
    const { loading } = this.state;

    return (
      <>
        {loading && <Spinner />}

        <iframe
          onLoad={this.onLoad}
          title="test"
          id="RSVPifyIFrame"
          style={{ width: '100%', border: 'none' }}
          src="//karenyaldo.app.rsvpify.com/?embed=1&js=1"
          frameBorder="0"
          scrolling="no"
        />
      </>
    );
  }
}

export default Rsvp;
