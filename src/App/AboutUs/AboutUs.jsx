// @flow
import React from 'react';

import { Quote } from 'components';

import karen from './karen_gray.jpg';
import aldo from './aldo_gray.jpg';

const AboutUs = () => (
  <div className="pa3 bg-near-white">
    <Quote
      quote="Cada mañana, cuando despierto, tú eres la razón por la que sonrío; tú eres la razón por la que amo."
      author="Jerry Burton"
    />

    <div className="flex flex-wrap">
      <div className="mv4 w-100 w-40-l flex flex-column items-center tc flex-grow-1">
        <img
          className="br-100 w5 h5 shadow-1 ba b--white bw3"
          src={karen}
          alt="Karen Vázquez"
        />
        <h2 className="mb0 baskerville i fw1 f2">Karen Vázquez</h2>
        <p className="measure f4">
          Es aventurera, y le gusta probar cosas nuevas. Tiene especial
          debilidad por la comida, y una sonrisa contagiosa.
        </p>
      </div>

      <svg
        className="light-red dn db-l"
        id="chart"
        width="60"
        height="500"
        viewBox="0 0 60 500"
      >
        <line strokeWidth="1" stroke="black" x1="30" y1="0" x2="30" y2="200" />
        <path
          fill="currentColor"
          transform="scale(2)"
          d="M15 116c-1.989-5.399-12-4.597-12 3.568 0 4.068 3.06 9.481 12 14.997 8.94-5.516 12-10.929 12-14.997 0-8.118-10-8.999-12-3.568z"
        />
        <line
          strokeWidth="1"
          stroke="black"
          x1="30"
          y1="300"
          x2="30"
          y2="500"
        />
      </svg>

      <div className="mv4 w-100 w-40-l flex flex-column items-center tc flex-grow-1">
        <img
          className="br-100 w5 h5 shadow-1 ba b--white bw3"
          src={aldo}
          alt="Aldo Funes"
        />
        <h2 className="mb0 baskerville i fw1 f2">Aldo Funes</h2>
        <p className="measure f4">
          Le gusta el montañismo y tomar fotos. A todos lados lleva su cámara y
          le encantan los libros de ciencia ficción.
        </p>
      </div>
    </div>
  </div>
);

export default AboutUs;
