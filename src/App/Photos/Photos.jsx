// @flow
import React, { Component } from 'react';

import { Spinner, Modal } from 'components';

import styles from './Photos.module.css';
import Photo from './Photo';

const HASHTAG = 'bodakarenyaldo';

type PhotoType = {|
  id: string,
  caption: string,
  url: string,
  accessibilityCaption: string
|};

type Props = {};
type State = {|
  loading: boolean,
  instagramPhotos: Array<PhotoType>,
  selectedPhoto: ?PhotoType
|};

class Photos extends Component<Props, State> {
  controller: AbortController;
  interval: IntervalID;

  componentDidMount() {
    if (window.AbortController) {
      this.controller = new window.AbortController();
    }
    this.loadInstagramPhotos();

    // this.interval = setInterval(() => this.loadInstagramPhotos(), 10000);
  }

  componentWillUnmount() {
    if (this.controller) {
      this.controller && this.controller.abort();
    }
    clearInterval(this.interval);
  }

  state = { loading: true, instagramPhotos: [], selectedPhoto: null };

  loadInstagramPhotos = () => {
    const url = new URL('/graphql/query/', 'https://www.instagram.com');
    url.searchParams.append('query_hash', 'f92f56d47dc7a55b606908374b43a314');
    url.searchParams.append(
      'variables',
      JSON.stringify({
        tag_name: HASHTAG,
        first: 128
      })
    );

    fetch(new Request(url.toString()), {
      signal: this.controller && this.controller.signal
    }).then(response => {
      if (response.status !== 200) {
        throw new Error('not 200');
      }

      response
        .json()
        .then(({ data: { hashtag: { edge_hashtag_to_media: { edges } } } }) => {
          this.setState({
            loading: false,
            instagramPhotos: edges.map(
              ({
                node: {
                  edge_media_to_caption: {
                    edges: [{ node: { text } = {} } = {}]
                  },
                  ...node
                }
              }) => ({
                id: node.id,
                caption: text,
                url: node.display_url,
                accessibilityCaption: node.accessibility_caption
              })
            )
          });
        });
    });
  };

  handlePhotoClick = (id: string) => {
    const { instagramPhotos } = this.state;

    this.setState({
      selectedPhoto: instagramPhotos.find(i => i.id === id)
    });
  };

  closeModal = () => {
    this.setState({ selectedPhoto: null });
  };

  selectedPhotoIndex = () => {
    const { instagramPhotos, selectedPhoto } = this.state;

    return instagramPhotos.indexOf(selectedPhoto);
  };

  handleNext = () => {
    const { instagramPhotos } = this.state;

    if (this.selectedPhotoIndex() === instagramPhotos.length - 1) {
      this.closeModal();
    }

    this.setState({
      selectedPhoto: instagramPhotos[this.selectedPhotoIndex() + 1]
    });
  };

  handlePrevious = () => {
    const { instagramPhotos } = this.state;

    if (this.selectedPhotoIndex() === 0) {
      this.closeModal();
    }

    this.setState({
      selectedPhoto: instagramPhotos[this.selectedPhotoIndex() - 1]
    });
  };

  render() {
    const { loading, instagramPhotos, selectedPhoto } = this.state;

    return (
      <section>
        <div className="ph3">
          <h2 className="tc avenir fw3 mv4">
            Etiqueta tus fotos en Instagram con{' '}
            <a
              className="link dim light-teal"
              target="_blank"
              rel="noopener noreferrer"
              href={`https://www.instagram.com/explore/tags/${HASHTAG}/`}
            >
              #{HASHTAG}
            </a>
          </h2>
        </div>

        {loading && <Spinner />}

        {selectedPhoto && (
          <Modal
            onNext={this.handleNext}
            onPrevious={this.handlePrevious}
            onClose={this.closeModal}
          >
            <img
              className="w-100"
              src={selectedPhoto.url}
              alt={selectedPhoto.accessibilityCaption}
            />
            <p>{selectedPhoto.caption}</p>
          </Modal>
        )}
        <div className={styles.photos}>
          {instagramPhotos
            .filter(({ id }) => !['2251354766917625801'].includes(id))
            .map(({ id, caption, url, accessibilityCaption }) => (
              <Photo
                key={id}
                id={id}
                caption={caption}
                url={url}
                accessibilityCaption={accessibilityCaption}
                onClick={this.handlePhotoClick}
              />
            ))}
        </div>
      </section>
    );
  }
}

export default Photos;
