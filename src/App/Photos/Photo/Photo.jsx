// @flow
import React, { memo } from 'react';

type Props = {
  id: string,
  url: string,
  caption: string,
  accessibilityCaption: string,
  onClick: (id: string) => void
};

const Photo = ({ id, url, caption, accessibilityCaption, onClick }: Props) => (
  <img
    onClick={() => onClick(id)}
    src={url}
    className="w-100 h-auto"
    alt={accessibilityCaption}
    title={caption}
  />
);

// $FlowFixMe
export default memo(Photo);
