// @flow
import 'react-app-polyfill/stable';
import 'tachyons';

import React from 'react';
import ReactDOM from 'react-dom';

import './index.css';
import App from './App/App';
import * as serviceWorker from './serviceWorker';

const rootElement = document.getElementById('root');

if (!rootElement) {
  throw new Error('Could not find #root element');
}

ReactDOM.render(<App />, rootElement);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
