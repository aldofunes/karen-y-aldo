// @flow
import React from 'react';

type Props = {
  quote: string,
  author: string
};

const Quote = ({ quote, author }: Props) => (
  <blockquote className="bl b--light-teal pl3 pv2 bw2 f3 i">
    {quote}
    <footer className="f5 mt2 gray">- {author}</footer>
  </blockquote>
);

export default Quote;
