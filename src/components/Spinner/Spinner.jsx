// @flow
import React from 'react';
import styles from './Spinner.module.css';

const Spinner = () => <div className={styles.ldsDualRing} />;

export default Spinner;
