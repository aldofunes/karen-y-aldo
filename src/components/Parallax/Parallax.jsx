// @flow
import React from 'react';
import cx from 'classnames';
import styles from './Parallax.module.css';

type Props = {
  className?: string,
  imageUrl: string
};

const Parallax = ({ className, imageUrl }: Props) => (
  <div
    className={cx('bg-center cover', className, styles.parallax)}
    style={{
      backgroundImage: `url(${imageUrl})`
    }}
  />
);

export default Parallax;
