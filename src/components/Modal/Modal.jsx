// @flow
import type { Node } from 'react';
import React, { Component } from 'react';

type Props = {
  children: Node,
  onClose: () => void,
  onNext: () => void,
  onPrevious: () => void
};

type State = {};

class Modal extends Component<Props, State> {
  overlay: any;

  static defaultProps = {
    onNext: () => {},
    onPrevious: () => {}
  };

  constructor(props: Props) {
    super(props);

    this.overlay = React.createRef();
  }

  componentDidMount() {
    this.overlay.current.focus();
  }

  handleKeyDown = (event: SyntheticKeyboardEvent<HTMLDivElement>) => {
    const { onClose, onNext, onPrevious } = this.props;
    if (event.key === 'Escape') {
      onClose();
    }

    if (event.key === 'ArrowRight') {
      onNext();
    }

    if (event.key === 'ArrowLeft') {
      onPrevious();
    }
  };

  handleContentClick = (event: SyntheticEvent<HTMLDivElement>) => {
    event.stopPropagation();
  };

  render() {
    const { children, onClose } = this.props;
    return (
      <div
        className="fixed z-999 left-0 top-0 bottom-0 right-0 overflow-auto bg-black-70 flex items-center justify-center pa2-m pa4-l"
        onClick={onClose}
        onKeyDown={this.handleKeyDown}
        ref={this.overlay}
        tabIndex="0"
      >
        <div className="bg-white" onClick={this.handleContentClick}>
          <button
            className="bg-none bn pa2 dim gray fr pointer"
            onClick={onClose}
          >
            <svg
              fill="none"
              height="24"
              stroke="currentColor"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              viewBox="0 0 24 24"
              width="24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <line x1="18" x2="6" y1="6" y2="18"></line>
              <line x1="6" x2="18" y1="6" y2="18"></line>
            </svg>
          </button>

          <div className="pa3">{children}</div>
        </div>
      </div>
    );
  }
}

export default Modal;
